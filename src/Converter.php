<?php

declare(strict_types=1);

namespace FileConverter;

class Converter
{
 
    private function jsonToJson(\SplFileObject $file, string $outputFormat, string $outputFilePath){
        $filewjsontojson = new \SplFileObject($outputFilePath, 'w');
        $line_of_text = array();
        while(!$file->eof())
        {
            $f = $file->fgets();
            $f = str_replace(" ", "", $f);
            $f = str_replace("\n", "", $f);
            $line_of_text[] = $f;
        }
        foreach ($line_of_text as $fields) {
            $filewjsontojson->fwrite($fields);
        }

    }
    private function csvToCsv(\SplFileObject $file, string $outputFormat, string $outputFilePath){
        $filewcsvtocsv = new \SplFileObject($outputFilePath, 'w');
        $line_of_text = array();
        while (!$file->eof()) {
            $line_of_text[] = $file->fgetcsv();
        }
        foreach ($line_of_text as $fields) {
            $filewcsvtocsv->fputcsv($fields);
        }
    }
    private function csvToJson(\SplFileObject $file, string $outputFormat, string $outputFilePath){
        $filewcsvtojson = new \SplFileObject($outputFilePath, 'w');
        $csv = array();
        while (!$file->eof())
        {
            $csv[] = $file->fgetcsv();
        }
        return $filewcsvtojson->fwrite(json_encode($csv));
    }
    public function convert(\SplFileObject $file, string $outputFormat, string $outputFilePath)
    {
        if($file -> getExtension() == "json" && $outputFormat == "json")
        {
            $filewjsontojson = $this->jsonToJson($file, $outputFormat, $outputFilePath);
            return $filewjsontojson;
        }
        if($file -> getExtension() == "csv" && $outputFormat == "csv")
        {
            $filewcsvtocsv = $this -> csvToCsv($file, $outputFormat, $outputFilePath);
            return $filewcsvtocsv;
        }
        if($file -> getExtension() == "csv" && $outputFormat == "json")
        {
            $filewcsvtojson = $this->csvToJson($file, $outputFormat, $outputFilePath);
            return $filewcsvtojson;
        }
    }
}